export interface UserData {
  info: {
    page: number,
    results: number,
    seed: String,
    version: String,
  },
  results: [
    {
      gender:	String,
      name: {
        title: String,
        first: String,
        last: String,
      },
      location: {
        street: {
          number: number,
          name: String,
        },
        city:	String,
        state: String,
        country: String,
        postcode:	String,
        coordinates: {
          latitude:	String,
          longitude: String,
        },
        timezone: {
          offset:	String,
          description: String,
        }
      },
      email: String,
      login: {
        uuid:	String,
        username:	String,
        password:	String,
        salt:	String,
        md5:	String,
        sha1:	String,
        sha256:	String,
      },
      dob: {
        date: String,
        age: number,
      },
      registered: {
        date: String,
        age:	number,
      },
      phone: String,
      cell: String,
      id: {
        name:	String,
        value?: String,
      },
      picture: {
        large: String,
        medium:	String,
        thumbnail: String,
      },
      nat: String,
    }
  ],
}