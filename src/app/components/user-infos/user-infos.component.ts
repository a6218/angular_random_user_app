import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user-service.service';

@Component({
  selector: 'app-user-infos',
  templateUrl: './user-infos.component.html',
  styleUrls: ['./user-infos.component.sass']
})
export class UserInfosComponent implements OnInit {
  firstName: String = 'Carla';
  lastName: String = 'Castro';
  email: String = 'carla.castro@example.com';
  picture: String = 'https://randomuser.me/api/portraits/women/88.jpg';
  isMale: boolean = false;

  constructor(private userSevice: UserService) { }

  ngOnInit(): void { }

  onClick() {
    this.getRandomUser();
  }

  getRandomUser() {
    this.userSevice.getUser().subscribe((user) => {
      let results = user.results[0];

      this.firstName = results.name.first;
      this.lastName = results.name.last;
      this.email = results.email;
      this.picture = results.picture.large;

      this.isMale = results.gender === 'male';
    });
  }
}
